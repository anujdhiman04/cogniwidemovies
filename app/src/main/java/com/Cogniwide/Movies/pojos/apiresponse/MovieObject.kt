package com.Cogniwide.Movies.pojos.apiresponse

import com.google.gson.annotations.SerializedName

object MovieObject {
    data class PopularMovies
    (
        @SerializedName("page")
        var page: Int?,
        @SerializedName("total_pages")
        var total_pages: Int?,
        @SerializedName("total_results")
        var total_results: Int?,
        @SerializedName("results")
        var results: ArrayList<Movie> = ArrayList(),
    )

    data class Movie(
        @SerializedName("title")
        var title: String?,
        @SerializedName("poster_path")
        var poster_path: String?,
    )
}