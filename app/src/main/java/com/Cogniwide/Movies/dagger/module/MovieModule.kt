package com.Cogniwide.Movies.dagger.module

import android.content.Context
import com.Cogniwide.Movies.repo.MovieRepository
import dagger.Module
import dagger.Provides

@Module
class MovieModule(val context: Context) {

    @Provides
    fun MovieRepository(): MovieRepository {
        return MovieRepository(context)
    }
}