package com.Cogniwide.Movies.dagger.appcomponent

import com.Cogniwide.Movies.dagger.module.MovieModule
import com.Cogniwide.Movies.repo.MovieRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MovieModule::class])
interface MovieRepositoryComponent {
    fun MovieRepository(): MovieRepository
}