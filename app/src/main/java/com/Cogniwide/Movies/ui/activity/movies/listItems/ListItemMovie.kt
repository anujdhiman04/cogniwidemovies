package com.Cogniwide.Movies.ui.activity.movies.listItems

import com.app.betterbuddys.util.ListItemModel
import com.app.betterbuddys.util.ViewTypes
import com.Cogniwide.Movies.pojos.apiresponse.MovieObject


class ListItemMovie (val movie: MovieObject.Movie): ListItemModel {

    override fun getViewType(): Int {
        return ViewTypes.movieList
    }

}