package com.Cogniwide.Movies.dagger.appcomponent

import com.Cogniwide.Movies.dagger.module.NetworkHelperModule
import com.Cogniwide.Movies.utils.NetworkHelper
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkHelperModule::class])
interface NetworkHelperComponent {
    fun NetworkHelper(): NetworkHelper
}