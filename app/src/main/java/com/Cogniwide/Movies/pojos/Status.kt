package com.Cogniwide.Movies.pojos

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}