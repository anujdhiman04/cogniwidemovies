package com.Cogniwide.Movies.pojos


data class Response<out T>(val status: Status, val data: T?, val message: String?, val responseCode: Int?) {
    companion object {

        fun <T> success(data: T?): Response<T> {
            return Response(Status.SUCCESS, data, null, 200)
        }

        fun <T> error(msg: String, data: T?, responseCode: Int?): Response<T> {
            return Response(Status.ERROR, data, msg, responseCode)
        }

        fun <T> loading(data: T?): Response<T> {
            return Response(Status.LOADING, data, null, 0)
        }

    }
}