package com.Cogniwide.Movies.ui.activity.movies

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AbsListView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.Cogniwide.Movies.R
import com.Cogniwide.Movies.databinding.ActivityMovieListBinding
import com.Cogniwide.Movies.pojos.Status
import com.Cogniwide.Movies.pojos.apiresponse.MovieObject
import com.Cogniwide.Movies.ui.activity.movies.listItems.ListItemMovie
import com.Cogniwide.Movies.ui.activity.movies.listItems.ViewRendererMovie
import com.Cogniwide.Movies.utils.ItemListAdapter
import com.Cogniwide.Movies.viewmodels.MovieViewModel
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener


class MovieListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMovieListBinding
    private var unFilterList = ArrayList<MovieObject.Movie>()

    public val movieViewModel: MovieViewModel by viewModels()
    private val itemListAdapter = ItemListAdapter()

    private var page: Int = 1
    private var needLoadMore = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieListBinding.inflate(layoutInflater)
        //setUserInformation()
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        binding.appBar.addOnOffsetChangedListener(object : OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    //showOption(R.id.action_info)
                } else if (isShow) {
                    isShow = false
                    //hideOption(R.id.action_info)
                }
            }
        })
        setToolbar()
        initUI()
        setObserver()
    }

    private fun initUI() {
        val layoutManager = GridLayoutManager(this, 2)
        binding.allMovies.setLayoutManager(layoutManager)

        //binding.allAlarms.setEmptyView(binding.root.findViewById(R.id.list_empty1))
        getMovies()

        binding.allMovies.adapter = itemListAdapter
        binding.allMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager1 = LinearLayoutManager::class.java.cast(recyclerView.layoutManager)

                val totalItemCount = layoutManager1!!.itemCount
                val lastVisible = layoutManager1.findLastVisibleItemPosition()

                val endHasBeenReached = lastVisible + 5 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached) {
                    //you have reached to the bottom of your recycler view
                    if (needLoadMore) {
                        Log.d("addOnScrollListener", "needLoadMore = $needLoadMore")
                        needLoadMore = false
                        //hit api for next page
                        //needLoadMore = false
                        getMovies()

                    }

                }
            }
        })
    }

    private fun getMovies() {
        movieViewModel.getPopularMovies(page)
    }

    fun setObserver() {
        movieViewModel.movieListResponse.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    showLoading(true)
                    binding.listEmpty1.hide()
                    binding.listEmpty1.text = resources.getString(R.string.list_empty)
                }
                Status.SUCCESS -> {
                    showLoading(false)
                    //unFilterList = it.data!!
                    if (page != it.data!!.total_pages){
                        page++
                        needLoadMore = true
                    }else{
                        needLoadMore = false
                    }
                    for (item in it.data.results) {
                        if (!unFilterList.contains(item)) {
                            unFilterList.add(item)
                        }
                    }

                    showToList(unFilterList)

                    binding.listEmpty1.hide()
                    binding.listEmpty1.text = resources.getString(R.string.list_empty)
                }
                Status.ERROR -> {
                    showLoading(false)
                    toast(it.message ?: "Something went wrong")
                    binding.listEmpty1.show()
                    binding.listEmpty1.text = it.message
                }
            }
        })

    }

    @SuppressLint("NewApi")
    private fun showToList(alarms: ArrayList<MovieObject.Movie>) {
        Log.d("showToList", "1")

        itemListAdapter.clear()
        Log.d("showToList", "2")

        for (alarm in alarms) {
            Log.d("showToList", "4")
            itemListAdapter.addListItem(
                ListItemMovie(alarm),
                ViewRendererMovie(this)
            )
        }
        Log.d("showToList", "3")

        Log.d("showToList", "5")
    }

    private fun showLoading(b: Boolean) {
        if (b) {
            binding.loading.show()
            binding.loading.isIndeterminate = true
        } else {
            binding.loading.hide()
        }
    }

    fun View.hide() {
        this.visibility = View.GONE
    }

    fun View.show() {
        this.visibility = View.VISIBLE
    }

    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun setToolbar() {
        binding.toolbarLayout.setContentScrimColor(Color.parseColor("#F7F7F7"))

/*
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
*/

        binding.toolbar.setTitleTextColor(
                ContextCompat.getColor(
                        this,
                        R.color.colorPrimaryDark

                )
        )
        binding.toolbar.title = getString(R.string.popular_movies)

/*
        binding.toolbar.navigationIcon =
            ContextCompat.getDrawable(this, R.drawable.ic__back)
*/


        //binding.toolbar.getNavigationIcon()!!.setColorFilter(Color.parseColor("#777777"), PorterDuff.Mode.SRC_ATOP);
;
    }



}