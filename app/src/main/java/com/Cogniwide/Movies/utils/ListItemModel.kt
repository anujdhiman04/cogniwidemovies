package com.app.betterbuddys.util

interface ListItemModel {
    fun getViewType(): Int
}