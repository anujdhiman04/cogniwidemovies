package com.Cogniwide.Movies.dagger.module

import android.content.Context
import com.Cogniwide.Movies.api.RestClient
import dagger.Module
import dagger.Provides

@Module
class RestClientModule(val context: Context) {

    @Provides
    fun RestClientRepository(): RestClient {
        return RestClient(context)
    }
}