package com.Cogniwide.Movies.api

import com.Cogniwide.Movies.pojos.apiresponse.MovieObject
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


public interface Webservices{
  @Headers("Accept: application/json")
  @GET(ApiURL.POPULAR_MOVIE_LIST)
  fun getPopularMovies(
          @Query("api_key") api_key: String,
          @Query("page") page: Int
  ): Single<MovieObject.PopularMovies>
}