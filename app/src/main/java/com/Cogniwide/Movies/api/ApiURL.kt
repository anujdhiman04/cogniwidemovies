package com.Cogniwide.Movies.api

object ApiURL {
    const val baseUrl: String = "https://api.themoviedb.org/3/"
    const val baseImageUrl: String = "https://image.tmdb.org/t/p/w500"
    const val API_KEY: String = "3f61123e4bce97a9db79a440ccc540ec"
    const val POPULAR_MOVIE_LIST: String = "movie/popular"

}