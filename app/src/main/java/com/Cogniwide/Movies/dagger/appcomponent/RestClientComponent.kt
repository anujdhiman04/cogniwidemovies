package com.Cogniwide.Movies.dagger.appcomponent

import com.Cogniwide.Movies.api.RestClient
import com.Cogniwide.Movies.dagger.module.RestClientModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RestClientModule::class])
interface RestClientComponent {
    fun RestClient(): RestClient
}