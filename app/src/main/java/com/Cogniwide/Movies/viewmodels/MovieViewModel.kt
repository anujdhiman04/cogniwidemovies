package com.Cogniwide.Movies.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.Cogniwide.Movies.dagger.appcomponent.DaggerMovieRepositoryComponent
import com.Cogniwide.Movies.dagger.module.MovieModule
import com.Cogniwide.Movies.repo.MovieRepository


public class MovieViewModel(application: Application) : AndroidViewModel(application) {

    private val movieRepository: MovieRepository = DaggerMovieRepositoryComponent.builder()
        .movieModule(MovieModule(application.applicationContext))
        .build().MovieRepository()

    val movieListResponse = movieRepository.moviesResponse
    //val LoadMore = movieRepository.LoadMore

    fun getPopularMovies(page: Int) {
        movieRepository.getPopularMovies(page)
    }

}