package com.Cogniwide.Movies.pojos

import com.google.gson.annotations.SerializedName

data class BaseResponse<out T>(
    @SerializedName("status")
    var status: Boolean = false,
    @SerializedName("mess")
    val mess: String?,
    @SerializedName("data")
    val  data: T?
){
    override fun toString(): String {
        return "BaseResponse(status=$status, mess=$mess, data=$data)"
    }
}
