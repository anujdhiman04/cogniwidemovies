package com.Cogniwide.Movies.dagger.module

import android.content.Context
import com.Cogniwide.Movies.utils.NetworkHelper
import dagger.Module
import dagger.Provides

@Module
class NetworkHelperModule(val context: Context) {

    @Provides
    fun NetworkHelperRepository(): NetworkHelper {
        return NetworkHelper(context)
    }
}