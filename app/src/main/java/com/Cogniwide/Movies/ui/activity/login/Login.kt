package com.Cogniwide.Movies.ui.activity.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.Cogniwide.Movies.R
import com.Cogniwide.Movies.databinding.ActivityLoginBinding
import com.Cogniwide.Movies.ui.activity.movies.MovieListActivity

class Login : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initUI()
        Showpasswrd()
        setTextWactcher()
        binding.txtinputemallayout.setEndIconOnClickListener {
            binding.email.text?.clear()
            binding.password.text?.clear()
        }
        binding.login.setOnClickListener {
            if (checkEmailValidity() && checkPasswordValidity()) {
                // good to go
                //Progress/loader to show
                binding.loading.visibility = View.VISIBLE
                binding.login.text = null
                binding.login.isClickable = false

                Handler().postDelayed(Runnable {

                    binding.loading.visibility = View.GONE
                    binding.login.text = getString(R.string.login)
                    binding.login.isClickable = true

                    startActivity(Intent(this, MovieListActivity::class.java))
                            .also {
                                finish()
                            }

                }, 2000)
            }
        }


    }

    private fun Showpasswrd() {

        binding.checkboxshowpassord.setOnCheckedChangeListener { compoundButton, value ->
            if (value) { // Show Password
                binding.password.transformationMethod =
                        HideReturnsTransformationMethod.getInstance()
            } else { // Hide Password
                binding.password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
    }

    private fun setTextWactcher() {

        binding.email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
            ) {
                binding.login.isClickable = checkEmailValidity() && checkPasswordValidity()
            }
        })
        binding.password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
            ) {
                binding.login.isClickable = checkEmailValidity() && checkPasswordValidity()
            }
        })

    }

    private fun initUI() {
        Handler().postDelayed({
            slideUp(binding.bottomLayout)
        }, 0)

    }

    private fun slideUp(view: View) {
        val transition = Slide(Gravity.BOTTOM)
        transition.duration = 500
        transition.addTarget(view)
        TransitionManager.beginDelayedTransition(binding.mainLayout, transition)
        view.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        binding.email.requestFocus()
    }


    //MARK: Custom Methods
    private fun checkEmailValidity(): Boolean {
        return if (binding.email.text.toString().matches(emailPattern.toRegex())) {
            true
        } else {
            binding.email.error = getString(R.string.email_error)
            false
        }
    }

    private fun checkPasswordValidity(): Boolean {
        return if (binding.password.text.toString().length >= 6 && binding.password.text.toString().length <= 12) {
            true
        } else {
            binding.password.error = getString(R.string.password_error)
            false
        }
    }
    //end of MARK
}