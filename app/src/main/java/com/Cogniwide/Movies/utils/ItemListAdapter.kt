package com.Cogniwide.Movies.utils

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.betterbuddys.util.ListItemModel
import com.app.betterbuddys.util.ViewRenderer

class ItemListAdapter(
    private var listItems: ArrayList<ListItemModel> = ArrayList(), //Will hold data and view type information
    private var viewRenderers: ArrayList<ViewRenderer<ListItemModel, RecyclerView.ViewHolder>> = ArrayList()
): RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    //Override Methods
    //RecyclerView.ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //viewType = position in the list
        return viewRenderers[viewType].createViewHolder(parent) /*?: ViewRendererDefault().createViewHolder(parent)*/
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun getItemViewType(position: Int): Int {
//        Old Code
//        return listItems[position].getViewType()
        return position
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        Log.d("LIA", "BindView for item $position")
        viewRenderers[position].bindView(
            listItems[position],
            holder,
            position
        )
    }



    fun <L: ListItemModel, V: ViewRenderer<ListItemModel, RecyclerView.ViewHolder>>
            addListItem(item: L, viewRenderer: V) : Int{
        if (!listItems.contains(item) && !viewRenderers.contains(viewRenderer)) {
            listItems.add(item)
            viewRenderers.add(viewRenderer)
//        Old Code
//        if (!viewRenders.containsKey(item.getViewType())){
//            viewRenders[item.getViewType()] = viewRenderer
//        }
            notifyItemChanged(listItems.size - 1)
            return listItems.size-1
        }
        return -1
    }
    fun <L: ListItemModel, V: ViewRenderer<ListItemModel, RecyclerView.ViewHolder>>
            setListItem(item: ArrayList<L>, viewRenderer: V){
        listItems.addAll(item)
        viewRenderers.add(viewRenderer)
//        Old Code
//        if (!viewRenders.containsKey(item.getViewType())){
//            viewRenders[item.getViewType()] = viewRenderer
//        }
        notifyItemChanged(listItems.size - 1)
    }


    fun getListItemAt(position: Int): ListItemModel{
        return listItems[position]
    }
    fun getList(): ArrayList<ListItemModel>{
        return listItems
    }

    fun clear(){
        listItems.clear()
        viewRenderers.clear()
        notifyDataSetChanged()
    }

}