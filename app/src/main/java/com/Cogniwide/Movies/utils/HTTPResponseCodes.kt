package com.Cogniwide.Movies.utils

class HTTPResponseCodes {
    companion object {
        const val CONTINUE: Int = 100
        const val SWITCH_PROTOCOLS: Int = 102
        const val OK: Int = 200
        const val CREATED: Int = 201
        const val UNAUTHORIZED: Int = 401
        const val FORBIDDEN: Int = 403
        const val NOT_ACCEPTABLE: Int = 409
        const val TOO_MANY_REQUESTS: Int = 429
        const val GENERAL_ERROR: Int = 422
        const val INTERNAL_SERVER_ERROR: Int = 500
        const val API_ERROR: Int = -1
        const val NOT_INTERNET: Int = -2
    }
}