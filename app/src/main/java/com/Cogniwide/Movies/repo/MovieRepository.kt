package com.Cogniwide.Movies.repo

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.Cogniwide.Movies.api.ApiURL
import com.Cogniwide.Movies.api.RestClient
import com.Cogniwide.Movies.dagger.appcomponent.DaggerNetworkHelperComponent
import com.Cogniwide.Movies.dagger.appcomponent.DaggerRestClientComponent
import com.Cogniwide.Movies.dagger.module.NetworkHelperModule
import com.Cogniwide.Movies.dagger.module.RestClientModule
import com.Cogniwide.Movies.pojos.Response
import com.Cogniwide.Movies.pojos.apiresponse.MovieObject
import com.Cogniwide.Movies.utils.HTTPResponseCodes
import com.Cogniwide.Movies.utils.NetworkHelper
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(val context: Context) {
    //private val restClient:RestClient=Dagg
    //(application as DakshApplication).getAppComponent()?.inject(this)

    private val webservice: RestClient = DaggerRestClientComponent.builder()
        .restClientModule(RestClientModule(context.applicationContext))
        .build().RestClient()
    private val networkHelper: NetworkHelper = DaggerNetworkHelperComponent.builder()
        .networkHelperModule(NetworkHelperModule(context.applicationContext))
        .build().NetworkHelper()

    private val _moviesResponse = MutableLiveData<Response<MovieObject.PopularMovies>>()
    val moviesResponse: LiveData<Response<MovieObject.PopularMovies>>
        get() = _moviesResponse

    fun getPopularMovies(page: Int) {
        _moviesResponse.postValue(Response.loading(null))
        if (networkHelper.isNetworkConnected()) {
            webservice.getPopularMovies(ApiURL.API_KEY, page)
                .timeout(10, TimeUnit.MINUTES)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.MINUTES)
                .subscribe(object : SingleObserver<MovieObject.PopularMovies> {
                    override fun onSubscribe(d: Disposable) {
                        //Not Used
                    }

                    override fun onSuccess(t: MovieObject.PopularMovies) {
                        if (t.results.isNotEmpty()) {
                            _moviesResponse.postValue(Response.success(t))
                        } else {
                            //error
                            _moviesResponse.postValue(
                                Response.error(
                                    "No Movie Found",
                                    null,
                                    HTTPResponseCodes.API_ERROR
                                )
                            )
                        }

                    }

                    override fun onError(e: Throwable) {
                        Log.d("LGVM", "Response Error:$e")
                        when (e) {
                            is HttpException -> {
                                //responseCode.value = e.code()
                                e.response()?.let {
                                    _moviesResponse.postValue(
                                        Response.error(
                                            it.message(),
                                            null,
                                            it.code()
                                        )
                                    )
                                }

                            }


                            else -> {
                                _moviesResponse.postValue(Response.error(e.message.toString(), null, -2))


                            }
                        }
                    }
                })
        }else{
            _moviesResponse.postValue(
                Response.error(
                    "No Internet Connection",
                    null,
                    HTTPResponseCodes.NOT_INTERNET
                )
            )

        }

    }

}