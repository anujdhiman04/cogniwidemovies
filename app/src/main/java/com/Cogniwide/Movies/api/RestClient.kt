package com.Cogniwide.Movies.api

import android.content.Context
import android.util.Log
import com.Cogniwide.Movies.pojos.apiresponse.MovieObject
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.Query
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RestClient @Inject constructor(context: Context) {

    private var retrofit: Retrofit
    private val TAG = RestClient::class.java.simpleName
    private var mGsonConverter: GsonConverterFactory? = null

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .writeTimeout(10, TimeUnit.MINUTES)
            .connectTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(ApiURL.baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .addConverterFactory(gsonConverter)
            .build()
    }

    val gsonConverter: GsonConverterFactory
        get() {
            if (mGsonConverter == null) {
                mGsonConverter = GsonConverterFactory
                    .create(
                        GsonBuilder()
                            .setLenient()
                            .disableHtmlEscaping()
                            .create()
                    )
            }
            return mGsonConverter!!
        }


    fun getPopularMovies(api_key: String, page: Int): Single<MovieObject.PopularMovies> {
        val webservices = retrofit.create<Webservices>(
            Webservices::class.java
        )
        return webservices.getPopularMovies(api_key, page)
    }
}