package com.app.betterbuddys.util

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class ViewRenderer<M: ListItemModel, H: RecyclerView.ViewHolder> {
    abstract fun bindView(model: M, holder: H, position: Int)

    abstract fun createViewHolder(parent: ViewGroup): H
}