package com.Cogniwide.Movies.ui.activity.movies.listItems
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.Cogniwide.Movies.databinding.MovieListitemBinding

class ViewHolderMovie(
    val parent: ViewGroup,
    viewBinding: MovieListitemBinding = MovieListitemBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )

): RecyclerView.ViewHolder(viewBinding.root) {
    val binding = viewBinding
}