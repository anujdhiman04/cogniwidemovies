package com.Cogniwide.Movies.ui.activity.movies.listItems

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.betterbuddys.util.ListItemModel
import com.app.betterbuddys.util.ViewRenderer
import com.Cogniwide.Movies.R
import com.Cogniwide.Movies.api.ApiURL
import com.squareup.picasso.Picasso


class ViewRendererMovie(
    private val context: Context
) : ViewRenderer<ListItemModel, RecyclerView.ViewHolder>() {
    //MARK: Properties
    private lateinit var holderMovie: ViewHolderMovie
    private lateinit var movieModel: ListItemMovie
    private var index: Int = -1

    @SuppressLint("SimpleDateFormat")
    override fun bindView(model: ListItemModel, holder: RecyclerView.ViewHolder, position: Int) {
        model as ListItemMovie
        holder as ViewHolderMovie

        holderMovie = holder
        movieModel = model
        index = position

        Picasso.get().load(ApiURL.baseImageUrl + movieModel.movie.poster_path).into(holderMovie.binding.ivMoviePoster)
        holderMovie.binding.tvTitle.text = movieModel.movie.title ?: "Title"

    }

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolderMovie(parent)
    }
}
